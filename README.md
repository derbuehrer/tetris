Mein Tetris ist open source und komplett kostenfrei.
Es können nach wie vor Bugs auftrehten, diese sollten aber nicht störend sein.

Zur Steuerung:
Mit A+D lassen sich fallende Tetrominos nach links bzw. rechts bewegen.
Mit S lassen sich Tetrominos schneller nach unten bewegen.
Mit M lassen sich die Tetrominos drehen.

Es gibt einen Debug Modus der mit F1 aktiviert und deaktiviert werden kann, dabei hören die Tetrominos auf zu fallen und lassen sich nun vollständig mit WASD und M bewegen.
Es wird aber empfolen den Debug Modus nicht zu verwenden, man will ja ehrlich seinen Highscore toppen :D, außerdem könnte es im Debug zu Fehlern kommen(...)

Viel Spaß mit meinem Tetris!