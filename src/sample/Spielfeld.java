package sample;

import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import java.util.ArrayList;

public class Spielfeld {

    // In koordinaten werden die einzelnen Blöcke gespeichert, so kann kein Block in einem anderen sein!
    // Das Spielfeld besteht aus 15 * 26 Koordinaten je 24 pixel groß
    public Block[][] koordinaten = new Block[15][26];
    public Block[][] next = new Block[7][7];
    public int count = 0;
    public AnchorPane spielefeld;
    private ArrayList tetros = new ArrayList<>();
    private AnchorPane nextfeld;
    private Label score;

    // Definiert das Spielfeld auf dem das Spiel stattfindet
    Spielfeld(AnchorPane spielefeld, AnchorPane next, Label score) {
        this.spielefeld = spielefeld;
        this.nextfeld = next;
        this.score = score;
        score.setText(String.valueOf(this.count));
    }

    public Block getKoordinate(int x, int y) {
        return koordinaten[x][y];
    }

    // Prüft ob eine Koordinate bereits belegt ist
    public boolean istBelegt(int x, int y) {
        if (x < 15 && y < 26 && x >= 0 && y >= 0 && koordinaten[x][y] == null) return false;
        return true;
    }

    public ArrayList getTetros() {
        return tetros;
    }

    // Setzt einen Block in die entsprechenden Koordinaten und hängt diesen in das Spielfeld ein (Fehler können auftrehten)
    public boolean setMinoInFeld(Tetromino mino, boolean feld) {
        tetros.add(mino);
        int[][] defkoordinaten = mino.getKoordinaten(feld);
        // gibt alle 4 Blöcke die zu einem Tetromino gehören im Spielfeld aus und speichert diese im Koordinatensystem
        for (int i = 0; i < 4; i++) {
            mino.getBlock(i).setKoordinate(defkoordinaten[i][0], defkoordinaten[i][1]);
            // setzt koordinatensystem und übergibt blöcke aus diesem in das ausgewählte Feld
            if (this.koordinaten[defkoordinaten[i][0]][defkoordinaten[i][1]] == null) {
                if (feld) {
                    this.koordinaten[defkoordinaten[i][0]][defkoordinaten[i][1]] = mino.getBlock(i);
                    spielefeld.getChildren().add(this.koordinaten[defkoordinaten[i][0]][defkoordinaten[i][1]]);
                }
                else {
                    this.next[defkoordinaten[i][0]][defkoordinaten[i][1]] = mino.getBlock(i);
                    nextfeld.getChildren().add(this.next[defkoordinaten[i][0]][defkoordinaten[i][1]]);
                }
            }
            else return false;
        }
        return true;
    }

    // aktualisiere Spielfeld
    public void aktualisiere() {
        Tetromino mino = (Tetromino) tetros.get(tetros.size() - 2);
        int x = 0;
        int y = 0;
        int zahl = 0;
        // Setze Koordinaten für Phantom zurück für erneuetes "fallen"
        for (int i = 0; i < 4; i++) {
            x = mino.getBlock(i).getKoordinate()[0];
            y = mino.getBlock(i).getKoordinate()[1];
            mino.getPhantom(i).setKoordinate(x,y);
        }
        // Das Phantom muss ähnlich wie das Tetromino fallen, jedoch viel schneller (am besten in einer Schleife)
        for (int i = 0; i < 26; i++) {
            // Prüfung ob Phantom "fallen" darf
            zahl = 0;
            for (int j = 0; j < 4; j++) {
                x = mino.getBlock(j).getKoordinate()[0];
                y = mino.getPhantom(j).getKoordinate()[1];
                mino.getPhantom(j).setKoordinate(x,y);
                if (y < 25 && (koordinaten[x][y + 1] == null || koordinaten[x][y + 1].getStatus())) {
                    zahl++;
                }
            }
            // Lässt Phantom "fallen"
            if (zahl == 4) {
                for (int j = 0; j < 4; j++) {
                    x = mino.getBlock(j).getKoordinate()[0];
                    y = mino.getPhantom(j).getKoordinate()[1];
                    if (y < 25 && (koordinaten[x][y + 1] == null || koordinaten[x][y + 1].getStatus())) {
                        y++;
                    }
                    mino.getPhantom(j).setKoordinate(x,y);
                    if (!spielefeld.getChildren().contains(mino.getPhantom(j))) spielefeld.getChildren().add(mino.getPhantom(j));
                }
            }
            else break;
        }
        spielefeld.getChildren();
    }

    // prüfe ob eine oder mehere Zeilen befüllt sind
    public void pruefe() {
        // Prüfzahlen ob Reihen befüllt sind
        int zahl = 0;
        int tetris = 0;
        // Prüfung ob Zeile befüllt ist
        for (int y = 0; y < 26; y++) {
            zahl = 0;
            for (int x = 0; x < 15; x++) {
                if (koordinaten[x][y] != null) zahl++;
            }
            // lösche Zeile wenn sie befüllt ist und lasse jeden obrigen Block um 1 koordinate fallen
            if (zahl == 15) {
                tetris++;
                for (int x = 0; x < 15; x++) {
                    spielefeld.getChildren().remove(koordinaten[x][y]);
                    koordinaten[x][y].setSichtbar(false);
                    koordinaten[x][y] = null;
                }
                for (; y >= 0; y--) {
                    for (int x = 0; x < 15; x++) {
                        if (koordinaten[x][y] != null) {
                            koordinaten[x][y].setKoordinate(x, y + 1);
                            koordinaten[x][y + 1] = koordinaten[x][y];
                            koordinaten[x][y] = null;
                        }
                    }
                }
                this.aktualisiere();
            }
        }
        // Vergebe Punkte abhängig von Zeilenlöschungen
        switch (tetris) {
            case 0 :
                count += 10;
                break;
            case 1 :
                count += 100;
                break;
            case 2 :
                count += 250;
                break;
            case 3 :
                count += 500;
                break;
            case 4 :
                count += 800;
                break;
        }
        if (tetris > 4) {
            count += tetris / 4 * 800;
        }
        // Gebe aktuellen score aus
        this.score.setText(String.valueOf(this.count));
    }
}