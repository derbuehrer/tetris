package sample;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import sample.Tetrominos.*;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

public class Tetris {

    public Label score;
    public Label levellabel;
    public AnchorPane spielefeld;
    public AnchorPane next;
    public Button startid;
    public Spielfeld feld;
    public Button stoppid;
    public Button pauseid;
    private Timeline timeline;
    private Timeline leveltimer;
    private int ms = 1000;
    private int level;
    // true = mit leveln && false = ohne level
    private boolean modus = true;
    private boolean debug = false;

    // Startet das Spiel
    public void start(ActionEvent actionEvent) {
        // Leveltimer für level, alle 30sekunden 2% schneller fallende Tetrominos
        leveltimer = new Timeline(new KeyFrame(
                Duration.seconds(30),
                ae -> {
                    ms *= 0.98;
                    timeline.setDelay(Duration.millis(ms));
                    level++;
                    levellabel.setText(String.valueOf(level));
                }));
        leveltimer.setCycleCount(Animation.INDEFINITE);

        // User fragen welchen Modus er spielen will (Leicht oder mit Leveln
        Alert input = new Alert(Alert.AlertType.CONFIRMATION);
        input.initStyle(StageStyle.UTILITY);
        input.setHeaderText("Willst du Mit Level oder im Babymodus spielen?");
        ButtonType btn0 = new ButtonType("Babymodus");
        ButtonType btn1 = new ButtonType("Level");
        input.getButtonTypes().remove(0,2);
        input.getButtonTypes().addAll(btn0, btn1);
        Optional<ButtonType> option = input.showAndWait();
        // Prüft nach Userauswahl
        if (option.get() != null) {
            if (option.get() == btn1) {
                leveltimer.play();
                modus = true;
            }
            if (option.get() == btn0) {
                modus = false;
            }
        }
        else return;

        // Startet das Spiel und verhindert einen weiteren Start
        startid.setDisable(true);
        feld = new Spielfeld(spielefeld, next, score);
        feld.setMinoInFeld((Tetromino) Functions.randomTetromino(Functions.randomFarbe()), true);
        feld.setMinoInFeld((Tetromino) Functions.randomTetromino(Functions.randomFarbe()), false);
        // Setze Timer welcher das Spiel regelt
        timeline = new Timeline(new KeyFrame(
                // sekündlich fällt das aktuelle tetromino herunter
                Duration.millis(ms),
                ae -> {
                    // Lasse Tetromino fallen bis es nicht mehr geht
                    Tetromino mino = (Tetromino) feld.getTetros().get(feld.getTetros().size() - 2);
                    if (mino.falle(feld)) feld.aktualisiere();
                        // Setze angekündigtes Tetromino und erzeuge neues zum ankündigen
                    else {
                        // Entfernt überschüssiges Tetromino aus Liste um Speicher zu sparen
                        feld.getTetros().remove(feld.getTetros().get(feld.getTetros().size() - 2));
                        feld.pruefe();
                        // Gameover wenn Tetromino nicht gesetzt werden kann
                        if (!feld.setMinoInFeld((Tetromino) feld.getTetros().get(feld.getTetros().size() - 1), true)) Functions.gameOver(timeline);
                        feld.setMinoInFeld((Tetromino) Functions.randomTetromino(Functions.randomFarbe()), false);
                    }
                }));
        timeline.setCycleCount(Animation.INDEFINITE);
        // WASD + ESC Tastenfunktion hinterlegt
        EventHandler<KeyEvent> eventHandler = new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                Tetromino tetromino = (Tetromino) feld.getTetros().get(feld.getTetros().size() - 2);
                KeyCode e = event.getCode();
                // Funktion je nach Tastendruck auslösen
                switch (e) {
                    case A:
                        tetromino.bewegeInRichtung( feld,false);
                        feld.aktualisiere();
                        break;
                    case D:
                        tetromino.bewegeInRichtung( feld,true);
                        feld.aktualisiere();
                        break;
                    case S:
                        if (tetromino.falle(feld)) {
                            feld.aktualisiere();
                        }
                        else {
                            // Entfernt überschüssiges Tetromino aus Liste um Speicher zu sparen
                            feld.getTetros().remove(feld.getTetros().get(feld.getTetros().size() - 2));
                            feld.pruefe();
                            // Gameover wenn Tetromino nicht gesetzt werden kann
                            if (!feld.setMinoInFeld((Tetromino) feld.getTetros().get(feld.getTetros().size() - 1), true)) Functions.gameOver(timeline);
                            feld.setMinoInFeld((Tetromino) Functions.randomTetromino(Functions.randomFarbe()), false);
                        }
                        break;
                    case M:
                        tetromino.drehen(feld);
                        feld.aktualisiere();
                        break;
                    case ESCAPE:
                        if (!debug) {
                            timeline.pause();
                            leveltimer.pause();
                            Alert input = new Alert(Alert.AlertType.INFORMATION);
                            input.initStyle(StageStyle.UTILITY);
                            input.setTitle("Pause");
                            input.setHeaderText("Das Spiel wurde Pausiert");
                            input.showAndWait();
                            if (modus) leveltimer.play();
                            timeline.play();
                        }
                        break;
                    // Debug
                    case F1:
                        if (debug) {
                            System.out.println("Deaktiviere Debug Modus");
                            debug = false;
                            timeline.play();
                        }
                        else {
                            System.out.println("Aktiviere Debug Modus");
                            debug = true;
                            timeline.pause();
                        }
                        break;
                    case W:
                        if (debug) {
                            tetromino.steige(feld);
                        }
                    case F2:
                        for (int x = 0; x < 15; x++) {
                            for (int y = 0; y < 26; y++) {
                                if (feld.istBelegt(x, y)) System.out.println("X: " + x + " Y: " + y);
                            }
                        }

                }
                // Fokus für Tastendruck liegt bei Spielfeld
                spielefeld.requestFocus();
            }
        };
        // Fokus für Tastendruck liegt bei Spielfeld aber aus reiner Vorsicht haben auch die anderen Elemente einen Eventhandler
        stoppid.addEventFilter(KeyEvent.KEY_PRESSED, eventHandler);
        pauseid.addEventFilter(KeyEvent.KEY_PRESSED, eventHandler);
        spielefeld.addEventFilter(KeyEvent.KEY_PRESSED, eventHandler);
        spielefeld.requestFocus();

        timeline.play();
    }

    // Beendet das Spiel
    public void stopp(ActionEvent actionEvent) {
        // Userabfrage ob dieser wirklich das Spiel beenden will
        Alert input = new Alert(Alert.AlertType.CONFIRMATION);
        input.initStyle(StageStyle.UTILITY);
        input.setTitle("Beenden?");
        input.setHeaderText("Willst du wirklich das Spiel beenden?");
        timeline.pause();
        leveltimer.pause();
        input.showAndWait();
        if (input.getResult().toString().contains("OK")) {
            // Das Spiel (das Programm) wird beendet
            System.exit(0);
        }
        else {
            // Das Spiel (das Programm) wird fortgesetzt
            timeline.play();
            if (modus) leveltimer.play();
        }
    }

    // Pausiert das Spiel
    public void pause(ActionEvent actionEvent) {
        // Pausiert und informiert mit einem Alert den Spieler über die Spielepause
        if (!debug){
            timeline.pause();
            leveltimer.pause();
            Alert input = new Alert(Alert.AlertType.INFORMATION);
            input.initStyle(StageStyle.UTILITY);
            input.setTitle("Pause");
            input.setHeaderText("Das Spiel wurde Pausiert");
            input.showAndWait();
            if (modus) leveltimer.play();
            timeline.play();
        }
    }
}