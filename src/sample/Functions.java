package sample;

import javafx.animation.Timeline;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.StageStyle;
import sample.Tetrominos.*;

import java.awt.*;

public class Functions {

    // Gibt gameover alert aus
    public static void gameOver(Timeline timeline) {
        // Muss noch Timer beenden und evt. Objekte löschen
        timeline.stop();
        Alert input = new Alert(Alert.AlertType.CONFIRMATION);
        input.initStyle(StageStyle.UTILITY);
        input.setTitle("Verloren lool");
        input.setHeaderText("Du bist Game Over!");
        input.showAndWait();
        System.exit(0);
    }

    // Wählt eine der 7 tetrisfarben aus und gib diese als Stringwert zurück
    public static String randomFarbe() {
        int rand = (int) ((Math.random() * 7) + 1);
        switch (rand) {
            case 1: return "cyan";
            case 2: return "blue";
            case 3: return "orange";
            case 4: return "yellow";
            case 5: return "green";
            case 6: return "magenta";
            case 7: return "red";
            default: return "green";
        }
    }

    // Wählt eine der 7 Tetrominoarten aus, erzeugt das Objekt und gibt dieses zurück
    public static Object randomTetromino(String farbe) {
        int rand = (int) ((Math.random()* 7) + 1);
        Object tetro = new Object();
        switch (rand) {
            case 1:
                tetro = new IMino(farbe);
                break;
            case 2:
                tetro = new JMino(farbe);
                break;
            case 3:
                tetro = new LMino(farbe);
                break;
            case 4:
                tetro = new OMino(farbe);
                break;
            case 5:
                tetro = new SMino(farbe);
                break;
            case 6:
                tetro = new TMino(farbe);
                break;
            case 7:
                tetro = new ZMino(farbe);
                break;
            default: tetro = new OMino(farbe);
        }
        return tetro;
    }
}