package sample;

import javafx.scene.control.Button;

public class Block extends Button {

    private String farbe = "black";
    private final int groesse = 24;
    private int[] koordinate = new int[2];
    private boolean fallend = true;
    private boolean sichtbar = true;

    // Legt Parameter für Block fest
    public Block(String farbe, int x, int y) {
        this.setKoordinate(x, y);
        this.farbe = farbe;
        this.setStyle("-fx-background-color: " + farbe);
        this.setMinSize(groesse, groesse);
        this.setMaxSize(groesse, groesse);


    }

    public String getFarbe() {
        return farbe;
    }

    public int[] getKoordinate() {
        return koordinate;
    }

    public void setKoordinate(int x, int y) {
        this.koordinate[0] = x;
        this.koordinate[1] = y;
        this.setLayoutX(x * groesse);
        this.setLayoutY(y * groesse);
    }

    public boolean istSichtbar() {
        return sichtbar;
    }

    public void setSichtbar(boolean sichtbar) {
        this.sichtbar = sichtbar;
    }

    public void setStatus(boolean status) {
        this.fallend = status;
    }

    public boolean getStatus() {
        return fallend;
    }
}
