package sample.Tetrominos;

import sample.Block;
import sample.Spielfeld;
import sample.Tetromino;

public class ZMino extends Tetromino {

    protected int[][] koordinaten = {{7,0}, {7,1}, {6,1}, {6,2}};

    public ZMino(String farbe) {
        super();
        for (int i = 0; i < 4; i++) {
            blocks[i] = new Block(farbe, koordinaten[i][0], koordinaten[i][1]);
            phantomblocks[i] = new Block(farbe, koordinaten[i][0], koordinaten[i][1]);
            phantomblocks[i].setDisable(true);
        }
    }

    @Override
    public void drehen(Spielfeld feld) {
        int x = 0;
        int y = 0;
        int aus = this.ausrichtung;
        /*
        0: -+ | ++ | +- | --
        1: fix
        2: ++ | +- | -- | -+
        3: += | =- | -= | =+
         */
        for (int i = 0; i < 3; i += 2) {
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            if (i == 0) {
                aus = this.ausrichtung;
            }
            else {
                if (aus < 3) aus += 1;
                else aus = 0;
            }
            switch (aus) {
                case 0 :
                    x -= 1;
                    y += 1;
                    break;
                case 1 :
                    x += 1;
                    y += 1;
                    break;
                case 2 :
                    x += 1;
                    y -= 1;
                    break;
                case 3 :
                    x -= 1;
                    y -= 1;
                    break;
            }
            if ((x < 0 || y < 0 || x >= 15 || y >= 26) || feld.koordinaten[x][y] != null && !feld.koordinaten[x][y].getStatus()) {
                return;
            }
        }
        x = this.getBlock(3).getKoordinate()[0];
        y = this.getBlock(3).getKoordinate()[1];
        feld.koordinaten[x][y] = null;
        switch (this.ausrichtung) {
            case 0 :
                x += 2;
                break;
            case 1 :
                y -= 2;
                break;
            case 2 :
                x -= 2;
                break;
            case 3 :
                y += 2;
                break;
        }
        if (feld.istBelegt(x,y)) {
            feld.koordinaten[this.getBlock(3).getKoordinate()[0]][this.getBlock(3).getKoordinate()[0]] = this.getBlock(3);
            return;
        }
        this.getBlock(3).setKoordinate(x,y);
        feld.koordinaten[x][y] = this.getBlock(3);
        // Block 0 und 2 werden gedreht
        for (int i = 2; i >= 0; i -= 2) {
            aus = this.ausrichtung;
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            feld.koordinaten[x][y] = null;
            if (i != 0) {
                if (aus < 3) aus += 1;
                else aus = 0;
            }
            switch (aus) {
                case 0 :
                    x -= 1;
                    y += 1;
                    break;
                case 1 :
                    x += 1;
                    y += 1;
                    break;
                case 2 :
                    x += 1;
                    y -= 1;
                    break;
                case 3 :
                    x -= 1;
                    y -= 1;
                    break;
            }
            this.getBlock(i).setKoordinate(x,y);
            feld.koordinaten[x][y] = this.getBlock(i);
        }
        if (this.ausrichtung < 3)this.ausrichtung++;
        else this.ausrichtung = 0;
    }

    @Override
    protected int[][] getNextfieldKoordinaten() {
        int[][] koordinaten = new int[][]{{4,1},{4,2},{3,2},{3,3}};
        return koordinaten;
    }

    @Override
    public void setKoordinaten(int[][] koordinaten) {
        this.koordinaten = koordinaten;
    }

    @Override
    public int[][] getKoordinaten(boolean feld) {
        if (feld) return this.koordinaten;
        return this.getNextfieldKoordinaten();
    }

    @Override
    public Block getBlock(int block) {
        return blocks[block];
    }

    @Override
    public Block getPhantom(int block) {
        return phantomblocks[block];
    }
}
