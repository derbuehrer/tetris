package sample.Tetrominos;

import sample.Block;
import sample.Spielfeld;
import sample.Tetromino;

public class OMino extends Tetromino {

    private int[][] koordinaten = {{6, 0}, {7, 0}, {6, 1}, {7, 1}};

    public OMino(String farbe) {
        super();
        for (int i = 0; i < 4; i++) {
            blocks[i] = new Block(farbe, koordinaten[i][0], koordinaten[i][1]);
            phantomblocks[i] = new Block(farbe, koordinaten[i][0], koordinaten[i][1]);
            phantomblocks[i].setDisable(true);
        }
    }

    @Override
    public void drehen(Spielfeld feld) {
        // Anfersich kann dieser nicht gedreht werden, bzw. es macht keinen unterschied
        blocks[0].setStyle("-fx-background-color: " + "red");
        blocks[1].setStyle("-fx-background-color: " + "green");
        blocks[2].setStyle("-fx-background-color: " + "blue");
        blocks[3].setStyle("-fx-background-color: " + "yellow");
        phantomblocks[0].setStyle("-fx-background-color: " + "red");
        phantomblocks[1].setStyle("-fx-background-color: " + "green");
        phantomblocks[2].setStyle("-fx-background-color: " + "blue");
        phantomblocks[3].setStyle("-fx-background-color: " + "yellow");
    }

    // Gibt die Standartkoordinaten des Tetrominos abhängig vom Spielefeldzurück
    @Override
    protected int[][] getNextfieldKoordinaten() {
        int[][] koordinaten = new int[][]{{2, 3}, {3, 3}, {2, 4}, {3, 4}};
        return koordinaten;
    }

    @Override
    public void setKoordinaten(int[][] koordinaten) {
        this.koordinaten = koordinaten;
    }

    @Override
    public int[][] getKoordinaten(boolean feld) {
        if (feld) return this.koordinaten;
        return this.getNextfieldKoordinaten();
    }

    @Override
    public Block getBlock(int block) {
        return blocks[block];
    }

    @Override
    public Block getPhantom(int block) {
        return phantomblocks[block];
    }
}
