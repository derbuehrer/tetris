package sample.Tetrominos;

import sample.Block;
import sample.Spielfeld;
import sample.Tetromino;

public class IMino extends Tetromino {

    protected int[][] koordinaten = {{5, 0}, {6, 0}, {7, 0}, {8, 0}};

    public IMino(String farbe) {
        super();
        for (int i = 0; i < 4; i++) {
            blocks[i] = new Block(farbe, koordinaten[i][0], koordinaten[i][1]);
            phantomblocks[i] = new Block(farbe, koordinaten[i][0], koordinaten[i][1]);
            phantomblocks[i].setDisable(true);
        }
    }

    // Dreht das Tetromino
    @Override
    public void drehen(Spielfeld feld) {
        int x = 0;
        int y = 0;
        int aus = this.ausrichtung;
        // Prüft ob gedreht werden kann
        for (int i = 0; i < 3; i += 2) {
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            if (i == 0) {
                aus = this.ausrichtung;
            }
            else {
                if (aus < 2)aus += 2;
                else aus -= 2;
            }
            switch (aus) {
                case 0 :
                    x += 1;
                    y += 1;
                    break;
                case 1 :
                    x += 1;
                    y -= 1;
                    break;
                case 2 :
                    x -= 1;
                    y -= 1;
                    break;
                case 3 :
                    x -= 1;
                    y += 1;
                    break;
            }
            if (feld.istBelegt(x, y)) return;
        }
        // Prüfe äußersten Block und setzt ihn abhängig von der Ausrichtung um
        x = this.getBlock(3).getKoordinate()[0];
        y = this.getBlock(3).getKoordinate()[1];
        feld.koordinaten[x][y] = null;
        switch (this.ausrichtung) {
            case 0 :
                x -= 2;
                y -= 2;
                break;
            case 1 :
                x -= 2;
                y += 2;
                break;
            case 2 :
                x += 2;
                y += 2;
                break;
            case 3 :
                x += 2;
                y -= 2;
                break;
        }
        // Setze Block wieder zurück wenn der Platz bereits belegt ist
        if (feld.istBelegt(x,y)) {
            feld.koordinaten[getBlock(3).getKoordinate()[0]][getBlock(3).getKoordinate()[1]] = this.getBlock(3);
            return;
        }
        feld.koordinaten[x][y] = this.getBlock(3);
        this.getBlock(3).setKoordinate(x,y);
        // Block 0 und 2 werden um Block 1 abhängig von Ausrichtung gedreht
        for (int i = 0; i < 3; i += 2) {
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            feld.koordinaten[x][y] = null;
            if (i == 0) {
                aus = this.ausrichtung;
            }
            else {
                if (aus < 2)aus += 2;
                else aus -= 2;
            }
            switch (aus) {
                case 0 :
                    x += 1;
                    y += 1;
                    break;
                case 1 :
                    x += 1;
                    y -= 1;
                    break;
                case 2 :
                    x -= 1;
                    y -= 1;
                    break;
                case 3 :
                    x -= 1;
                    y += 1;
                    break;
            }
            this.getBlock(i).setKoordinate(x,y);
            feld.koordinaten[x][y] = this.getBlock(i);
        }
        if (this.ausrichtung < 3)this.ausrichtung++;
        else this.ausrichtung = 0;
    }

    @Override
    protected int[][] getNextfieldKoordinaten() {
        int[][] koordinaten = new int[][]{{2,3},{3,3},{4,3},{5,3}};
        return koordinaten;
    }

    @Override
    public void setKoordinaten(int[][] koordinaten) {
        this.koordinaten = koordinaten;
    }

    @Override
    public int[][] getKoordinaten(boolean feld) {
        if (feld) return this.koordinaten;
        return this.getNextfieldKoordinaten();
    }

    @Override
    public Block getBlock(int block) {
        return blocks[block];
    }

    @Override
    public Block getPhantom(int block) {
        return phantomblocks[block];
    }
}
