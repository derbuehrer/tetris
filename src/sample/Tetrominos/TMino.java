package sample.Tetrominos;

import sample.Block;
import sample.Spielfeld;
import sample.Tetromino;

public class TMino extends Tetromino {

    protected int[][] koordinaten = {{5,0}, {6,0}, {7,0}, {6,1}};

    public TMino(String farbe) {
        super();
        for (int i = 0; i < 4; i++) {
            blocks[i] = new Block(farbe, koordinaten[i][0], koordinaten[i][1]);
            phantomblocks[i] = new Block(farbe, koordinaten[i][0], koordinaten[i][1]);
            phantomblocks[i].setDisable(true);
        }
    }

    @Override
    public void setKoordinaten(int[][] koordinaten) {
        this.koordinaten = koordinaten;
    }

    @Override
    public int[][] getKoordinaten(boolean feld) {
        if (feld) return this.koordinaten;
        return this.getNextfieldKoordinaten();
    }

    @Override
    public void drehen(Spielfeld feld) {
        int x = 0;
        int y = 0;
        int aus = this.ausrichtung;
        /*
        0: ++ | +- | -- | -+
        1: fix
        2: -- | -+ | ++ | +-
        3: +- | -- | -+ | ++
         */
        for (int i = 0; i < 4; i++) {
            aus = this.ausrichtung;
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            if (i == 1) continue;
            else if (i == 2) {
                if (aus < 2) aus += 2;
                else aus -= 2;
            }
            else if (i == 3) {
                if (aus < 3) aus += 1;
                else aus = 0;
            }
            switch (aus) {
                case 0 :
                    x += 1;
                    y += 1;
                    break;
                case 1 :
                    x += 1;
                    y -= 1;
                    break;
                case 2 :
                    x -= 1;
                    y -= 1;
                    break;
                case 3 :
                    x -= 1;
                    y += 1;
                    break;
            }
            if ((x < 0 || y < 0 || x >= 15 || y >= 26) || feld.koordinaten[x][y] != null && !feld.koordinaten[x][y].getStatus()) {
                return;
            }
        }
        // drehe Blöcke um fixen Block (index = 1)
        for (int i = 0; i < 4; i++) {
            aus = this.ausrichtung;
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            feld.koordinaten[x][y] = null;
            if (i == 1) continue;
            else if (i == 2) {
                if (aus < 2) aus += 2;
                else aus -= 2;
            }
            else if (i == 3) {
                if (aus < 3) aus += 1;
                else aus = 0;
            }
            switch (aus) {
                case 0 :
                    x += 1;
                    y += 1;
                    break;
                case 1 :
                    x += 1;
                    y -= 1;
                    break;
                case 2 :
                    x -= 1;
                    y -= 1;
                    break;
                case 3 :
                    x -= 1;
                    y += 1;
                    break;
            }
            this.getBlock(i).setKoordinate(x,y);
        }
        // Aktualisiere Koordinatensystem
        for (int i = 0; i < 4; i++) {
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            feld.koordinaten[x][y] = this.getBlock(i);
        }
        if (this.ausrichtung < 3)this.ausrichtung++;
        else this.ausrichtung = 0;
    }

    @Override
    protected int[][] getNextfieldKoordinaten() {
        int[][] koordinaten = new int[][]{{2,1},{3,1},{4,1},{3,2}};
        return koordinaten;
    }

    @Override
    public Block getBlock(int block) {
        return blocks[block];
    }

    @Override
    public Block getPhantom(int block) {
        return phantomblocks[block];
    }
}
