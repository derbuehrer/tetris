package sample;

public abstract class Tetromino {

    protected int art = 0;
    protected int ausrichtung = 0;
    protected int[][] koordinaten = {{0, 0}, {0, 0}, {0, 0}, {0, 0}};
    protected Block[] blocks = new Block[4];
    protected Block[] phantomblocks = new Block[4];

    public Tetromino() {
    }

    public abstract Block getBlock(int block);
    public abstract Block getPhantom(int block);

    public abstract void drehen(Spielfeld feld);

    public boolean steige(Spielfeld feld) {
        int[][] minokoordinaten = new int[4][2];
        int x = 0;
        int y = 0;
        // Prüfung ob alle Blöcke weiter fallen können
        for (int i = 0; i < 4; i++) {
            y = this.getBlock(i).getKoordinate()[1];
            if (y - 1 < 0) {
                return false;
            }
        }
        // für jeden block innerhalb des Tetrominos die Koordinaten 1 nach unten setzen
        for (int i = 3; i >= 0; i--) {
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            // setze Block um 24 pixel (1koordinate) nach unten und lösche aktuelle position
            feld.koordinaten[x][y] = null;
            --y;
            // setze aktualisierte koordinate des blockes (in das koordinatensystem)
            this.getBlock(i).setKoordinate(x, y);
            minokoordinaten[i][0] = x;
            minokoordinaten[i][1] = y;
        }
        for (int i = 0; i < 4; i++) {
            if (this.getBlock(i).istSichtbar()) {
                x = this.getBlock(i).getKoordinate()[0];
                y = this.getBlock(i).getKoordinate()[1];
                feld.koordinaten[x][y] = this.getBlock(i);
            }
        }
        this.setKoordinaten(minokoordinaten);
        return true;
    }

    // Lässt das Tetromino fallen
    public boolean falle(Spielfeld feld) {
        int[][] minokoordinaten = new int[4][2];
        int x = 0;
        int y = 0;
        // Prüfung ob alle Blöcke weiter fallen können
        for (int i = 0; i < 4; i++) {
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            if (this.getBlock(i).istSichtbar()) {
                if (y == 25 || (feld.koordinaten[x][y + 1] != null && !feld.getKoordinate(x, y + 1).getStatus())) {
                    // Setzt Status auf false und entfernt Phantom wenn Tetromino nicht weiter fallen kann
                    for (int j = 0; j < 4; j++) {
                        this.getBlock(j).setStatus(false);
                        feld.spielefeld.getChildren().remove(this.getPhantom(j));
                    }
                    return false;
                }
            }
        }
        // für jeden block innerhalb des Tetrominos die Koordinaten 1 nach unten setzen
        for (int i = 0; i < 4; i++) {
            if (this.getBlock(i).istSichtbar()) {
                x = this.getBlock(i).getKoordinate()[0];
                y = this.getBlock(i).getKoordinate()[1];
                feld.koordinaten[x][y] = null;
                ++y;
                // setze aktualisierte koordinate des blockes (in das koordinatensystem)
                this.getBlock(i).setKoordinate(x, y);
                minokoordinaten[i][0] = x;
                minokoordinaten[i][1] = y;
            }
        }
        // Aktualisiert Koordinatensystem
        for (int i = 0; i < 4; i++) {
            if (this.getBlock(i).istSichtbar()) {
                x = this.getBlock(i).getKoordinate()[0];
                y = this.getBlock(i).getKoordinate()[1];
                feld.koordinaten[x][y] = this.getBlock(i);
            }
        }
        this.setKoordinaten(minokoordinaten);
        return true;
    }

    // Bewegt Tetromino nach Rechts oder Links
    public void bewegeInRichtung(Spielfeld feld, boolean richtung) {
        int[][] minokoordinaten = new int[4][2];
        int x = 0;
        int y = 0;
        // Prüfung ob der Zug Legitim ist (ist Platz frei)
        for (int i = 0; i < 4; i++) {
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            // Prüfung für Rechts und für Links
            if (richtung) {
                if (!(x + 1 < 15 && (feld.koordinaten[x + 1][y] == null || feld.koordinaten[x + 1][y].getStatus()))) return;
            }
            else {
                if (!(x - 1 >= 0 && (feld.koordinaten[x - 1][y] == null || feld.koordinaten[x - 1][y].getStatus()))) return;
            }
        }
        // Versetzt nach erfolgreicher Prüfung die Blöcke nach rechts oder links
        for (int i = 0; i < 4; i++) {
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            feld.koordinaten[x][y] = null;
            // nach rechts oder links
            if (richtung) {
                ++x;
            }
            else {
                --x;
            }
            // setze aktualisierte Koordinaten (in das Koordinatensystem)
            this.getBlock(i).setKoordinate(x, y);
            minokoordinaten[i][0] = x;
            minokoordinaten[i][1] = y;
        }
        // Aktualisiert Koordinatensystem
        for (int i = 0; i < 4; i++) {
            x = this.getBlock(i).getKoordinate()[0];
            y = this.getBlock(i).getKoordinate()[1];
            feld.koordinaten[x][y] = this.getBlock(i);
        }
        this.setKoordinaten(minokoordinaten);
    }

    protected abstract int[][] getNextfieldKoordinaten();

    public abstract void setKoordinaten(int[][] koordinaten);

    public abstract int[][] getKoordinaten(boolean feld);
}